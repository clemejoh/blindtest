import pyglet
import time
import datetime

from os import listdir
from os.path import isfile, join

from difflib import SequenceMatcher

from mutagen.mp3 import MP3

VOLUME_MAX = 0.2

class LCG:
    def __init__(self, seed):
        self.m = 2**31 - 1
        self.a = 1664525
        self.c = 1013904223
        self.seed = seed

    def random(self, M):
        self.seed = (self.a * self.seed + self.c) % self.m
        return self.seed % M

def clean(s,lower=True):
    res = s
    if lower : res = s.lower()
    return res.replace("_"," ")

def score_compare(str1, str2):
    res = SequenceMatcher(None, clean(str1), clean(str2)).ratio()
    if res > 0.8 : return 1
    else :         return 0

def get_name(path_name):
    '''Asssume ending with .xxx'''
    k = -5
    while path_name[k] != '/':
        k -= 1
    return path_name[k+1:-4]

def list_files(dossier):
    onlyfiles = sorted([join(dossier,f).replace('\\','/') for f in listdir(dossier) if f.endswith('.mp3')])
    return onlyfiles

START      = -1
GUESS      = 0
LISTENING  = 1
WAIT       = 2
END        = 3

class Game :
    def __init__(self):
        self.nb_tours = 10
        self.guessing_time = 20
        self.listening_time = 7
        self.pause_time = 7
        self.dossier = 'Ado/'
        self.guessed = 0
        self.guess_time = 0
        self.ans_file = None
        self.player_name = ""
        self.list_songs = None

        self.gen = None
        
        # Music Player
        self.player = pyglet.media.Player()
        self.player.volume = 0.1

        self.volume = 0.1
        self.volume_attenuation = 1
        self.fade_time = 1

        self.seed = 123456789

        self.time_begin = 0

        self.song = ""
        self.state = GUESS
        self.update_state = None

        self.results = []

    def write_result(self):
        self.ans_file.write(f'{self.guessed}\n{self.guess_time}\n')
        
    def close_ans(self):
        self.ans_file.close()

    def update_volume(self):
        self.player.volume = self.volume * self.volume_attenuation
        
    def fade(self, dt):
        self.volume_attenuation -= 0.01 / self.fade_time
        self.volume_attenuation = max(0, self.volume_attenuation)
        self.update_volume()
        
    def begin_fade(self, dt):
        pyglet.clock.schedule_interval_for_duration(self.fade, 0.01, self.fade_time-0.02)
        
    def guess_stop(self, dt):
        self.results.append((self.song, self.guessed == 1, self.guess_time))
        self.write_result()
        self.state = LISTENING
        self.update_state()
        pyglet.clock.schedule_once(self.begin_fade,
                                   self.listening_time - self.fade_time)
        
    def play_stop(self, dt):
        self.player.next_source()
        if self.nb_tours > 0 :
            self.state = WAIT
        else :
            self.state = END
        self.update_state()

    def play_audio(self, fichier):
        sound = pyglet.media.load(fichier)
        self.player.queue(sound)
        self.player.play()
        d = int(MP3(fichier).info.length)
        d -= self.guessing_time + self.listening_time
        pos = self.gen.random(100)*d/100
        self.player.seek(pos)
    
    def play_random(self):
        nb_f = len(self.list_songs)
        id_song = self.gen.random(nb_f)
        self.play_audio(self.list_songs[id_song])
        song_name = get_name(self.list_songs[id_song])
        self.list_songs.pop(id_song)
        return song_name
    
    def next_song(self, dt):
        if self.nb_tours > 0 and len(self.list_songs) > 0:
            self.guessed = 0
            self.time_begin = time.time()
            self.song = self.play_random()
            self.state = GUESS

            self.volume_attenuation = 1
            self.update_volume()
            
            self.update_state()
            
            pyglet.clock.schedule_once(self.guess_stop,
                                       self.guessing_time)
            pyglet.clock.schedule_once(self.play_stop,
                                       self.guessing_time + self.listening_time)
            pyglet.clock.schedule_once(self.next_song,
                                       self.guessing_time + self.listening_time + self.pause_time)
            self.nb_tours -= 1
        else:
            self.state = END
            self.update_state()

    def start(self, dt):
        self.list_songs = list_files(self.dossier)
        self.ans_file = open(self.player_name + ".txt", 'w')
        self.ans_file.write(f'{self.nb_tours}\n')
        self.gen = LCG(self.seed)
        self.next_song(1)

class GameWindow(pyglet.window.Window):
    def __init__(self, game):
        super(GameWindow, self).__init__(540, 500,
                                         caption='Blind Test',
                                         resizable=True)
        self.game = game
        self.game.update_state = self.update_state

        bar = pyglet.resource.image('bar.png')
        knob = pyglet.resource.image('knob.png')
        depressed = pyglet.resource.image('button_up.png')
        pressed = pyglet.resource.image('button_down.png')
        hover = pyglet.resource.image('button_hover.png')

        self.batch = pyglet.graphics.Batch()
        pyglet.gl.glClearColor(0.8, 0.8, 0.8, 1.0)
        self.frame = pyglet.gui.Frame(self, order=4)

        # Home widgets
        self.home_group = pyglet.graphics.Group(order=0)
        self.name_entry = pyglet.gui.TextEntry("Nom", 150, 400, 200, group=self.home_group, batch=self.batch)
        self.push_handlers(self.name_entry)

        self.seed_text = pyglet.text.Label("Graîne", x=50, y=350, batch=self.batch, group=self.home_group, color=(0, 0, 0, 255))
        self.seed_entry = pyglet.gui.TextEntry("", 250, 350, 200, group=self.home_group, batch=self.batch)
        self.push_handlers(self.seed_entry)

        self.tours_text = pyglet.text.Label("Nombre de tours", x=50, y=300, batch=self.batch, group=self.home_group, color=(0, 0, 0, 255))
        self.tours_entry = pyglet.gui.TextEntry("5", 250, 300, 200, group=self.home_group, batch=self.batch)
        self.push_handlers(self.tours_entry)

        self.guessing_time_text = pyglet.text.Label("Temps de devination", x=50, y=250, batch=self.batch, group=self.home_group, color=(0, 0, 0, 255))
        self.guessing_time_entry = pyglet.gui.TextEntry(str(self.game.guessing_time), 250, 250, 200, group=self.home_group, batch=self.batch)
        self.push_handlers(self.guessing_time_entry)

        self.listening_time_text = pyglet.text.Label("Temps d'écoute supp.", x=50, y=200, batch=self.batch, group=self.home_group, color=(0, 0, 0, 255))
        self.listening_time_entry = pyglet.gui.TextEntry(str(self.game.listening_time), 250, 200, 200, group=self.home_group, batch=self.batch)
        self.push_handlers(self.listening_time_entry)

        self.pause_time_text = pyglet.text.Label("Temps d'interlude", x=50, y=150, batch=self.batch, group=self.home_group, color=(0, 0, 0, 255))
        self.pause_time_entry = pyglet.gui.TextEntry(str(self.game.pause_time), 250, 150, 200, group=self.home_group, batch=self.batch)
        self.push_handlers(self.pause_time_entry)

        self.playlist_text = pyglet.text.Label("Playlist", x=50, y=100, batch=self.batch, group=self.home_group, color=(0, 0, 0, 255))
        self.playlist_entry = pyglet.gui.TextEntry("", 250, 100, 200, group=self.home_group, batch=self.batch)
        self.push_handlers(self.playlist_entry)

        self.date_text = pyglet.text.Label("Heure de départ", x=50, y=50, batch=self.batch, group=self.home_group, color=(0, 0, 0, 255))

        self.date_entry = pyglet.gui.TextEntry("0", 250, 50, 50, group=self.home_group, batch=self.batch)
        self.push_handlers(self.date_entry)

        self.button = pyglet.gui.ToggleButton(400, 35, pressed=pressed, depressed=depressed, hover=hover, batch=self.batch, group=self.home_group)
        self.button.set_handler('on_toggle', self.launch_game_handler)
        self.frame.add_widget(self.button)
        
        # Game widgets        
        self.game_group = pyglet.graphics.Group(order=1)
        self.text_state = pyglet.text.Label("Bonjour", x=50, y=300, batch=self.batch, group=self.game_group, color=(0, 0, 0, 255))

        self.slider = pyglet.gui.Slider(100, 200, bar, knob, edge=5, group=self.game_group, batch=self.batch)
        self.slider.set_handler('on_change', self.slider_handler)
        self.slider.value = self.game.volume*100/VOLUME_MAX;
        self.frame.add_widget(self.slider)
        
        self.slider_label = pyglet.text.Label("Volume", x=300, y=200, group=self.game_group, batch=self.batch, color=(0, 0, 0, 255))
    
        self.text_entry = pyglet.gui.TextEntry("", 100, 100, 150, group=self.game_group, batch=self.batch)
        self.push_handlers(self.text_entry)
        self.text_entry.set_handler('on_commit', self.text_entry_handler)
        self.text_entry_label = pyglet.text.Label("", x=300, y=100, group=self.game_group, batch=self.batch, color=(0, 0, 0, 255))

        # Results
        self.end_group = pyglet.graphics.Group(order=2)
        self.end_widgets_list = []
        self.end_result_begin = 0
        self.end_showed = False
        
        self.game_group.visible = False
        self.end_group.visible = False
        self.launched = False

    def new_result(self, i, n):
        if self.game.results[i][1]: col = (0,255,0)
        else: col = (255,0,0)
        circle = pyglet.shapes.Circle(100, 405 - i*50, 10, color=col, batch=self.batch, group=self.end_group)
        song_name = pyglet.text.Label(self.game.results[i][0], x = 120, y = 400-i*50, group=self.end_group, batch=self.batch,color=(0,0,0,255))
        time_text = pyglet.text.Label(str(round(self.game.results[i][2],3)), x = 320, y = 400-i*50, group=self.end_group, batch=self.batch,color=(0,0,0,255))
        if not self.game.results[i][1]: time_text.color=(0,0,0,0)
        self.end_widgets_list.append((circle,song_name,time_text))

    def update_result(self):
        for i in range(len(self.end_widgets_list)):
            (circle, song_name, time_text) = self.end_widgets_list[i]
            circle.y = 405 - (i-self.end_result_begin)*50
            song_name.y = 400-(i-self.end_result_begin)*50
            time_text.y = 400-(i-self.end_result_begin)*50

    def on_mouse_scroll(self, x, y, scroll_x, scroll_y) :
        if self.game.state == END:
            self.end_result_begin += scroll_y
            self.update_result()
        
    def on_draw(self):
        self.clear()
        self.batch.draw()

    def on_close(self):
        self.game.player.pause()
        self.close()

    def slider_handler(self,value):
        self.game.volume = value/100*VOLUME_MAX
        self.game.update_volume()

    def text_entry_handler(self,text):
        if self.game.state == GUESS:
            self.game.guessed = score_compare(self.game.song,text)
            self.game.guess_time = time.time() - self.game.time_begin
            self.text_entry_label.text = text
            self.text_entry.value = ""

    def launch_game_handler(self, val):
        if self.launched == False:
            self.game.player_name = self.name_entry.value
            self.game.seed = int(self.seed_entry.value)
            if self.tours_entry.value.isdigit():
                self.game.nb_tours = int(self.tours_entry.value)
            if self.guessing_time_entry.value.isdigit():
                self.game.guessing_time = int(self.guessing_time_entry.value)
            if self.listening_time_entry.value.isdigit():
                self.game.listening_time = int(self.listening_time_entry.value)
            if self.pause_time_entry.value.isdigit():
                self.game.pause_time = int(self.pause_time_entry.value)
            self.game.dossier = self.playlist_entry.value + '/'

            date_str = self.date_entry.value
            if date_str == "0":
                to_wait = 0
            else:
                actual = datetime.datetime.now()
                hour = int(date_str.split(":")[0])
                minutes = int(date_str.split(":")[1])
                start = datetime.datetime(actual.year, actual.month, actual.day, hour, minutes)
                to_wait = (start-actual).total_seconds()

            self.home_group.visible = False
            self.game_group.visible = True
            self.text_state.text = f"On attend {int(to_wait)} secondes"
            pyglet.clock.schedule_once(self.game.start,to_wait)
        self.launched = True

    def update_state(self):
        if self.game.state == GUESS :
            self.text_state.text = "C'est l'heure de jouer !"
            self.text_entry_label.text = ""
        if self.game.state == LISTENING :
            if self.game.guessed == 1:
                self.text_state.text = "Bonne réponse ! "
            else :
                self.text_state.text = "Mauvaise réponse ! "
            self.text_state.text += f"La réponse était {clean(self.game.song,False)} !"

        if self.game.state == WAIT :
            self.text_state.text = "La chenille va repartir..."
        if self.game.state == END :
#            self.text_state.text = "Le jeu est fini :'("
            self.end_group.visible = True
            self.game_group.visible = False
            if not self.end_showed :
                n = len(self.game.results)
                for i in range(n):
                    self.new_result(i,n)
                self.end_showed = True


def main():    
    game = Game()
    window = GameWindow(game)

    pyglet.app.run()
    game.close_ans()
    
main()
