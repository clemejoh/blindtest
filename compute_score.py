import numpy as np
from os import listdir

def get_name(path_name):
    '''Asssume ending with .txt'''
    return path_name[:-4]

def read():
    matrix = [] # nb joueurs x couples (float,float) nb songs.
    players = [] # list of players name
    onlyfiles = [f for f in listdir(dossier) if f.endswith('.txt')]
    for file_name in onlyfiles: # en ligne les joueurs, en colonne les songs
        player = [] 
        file = open(file_name, 'r')
        nb_songs = int(file.readline())
        for _ in range(nb_songs):
            attempt = float(file.readline())
            ans_time = float(file.readline())
            player.append((attempt,ans_time))
        matrix.append(player)
        players.append(get_name(file_name))
         
    return np.array(matrix), players, nb_songs

def score_fct(guess_score, rank):
    rank_score = 1
    if rank == 0:
        rank_score = 2
    if rank == 1:
        rank_score = 1
    return guess_score*rank_score

def score_song(matrix,nb_players,nb_songs):
    '''Return array st arr[i] is the score of player i'''
    scores = np.zeros(nb_players)
    for i in range(nb_songs):
        list_tuples = matrix[:,i,:].reshape((nb_players,2)) # tuple = (guess_score,guess_time)
        list_times = list_tuples[:,1] + 9999*(np.ones(np.shape(list_tuples[:,1]))-list_tuples[:,0])
        ord = np.argsort(list_times)
        for player in range(nb_players):
            rank = ord[player]
            guess_score = list_tuples[player][0]
            scores[player] += score_fct(guess_score, rank)
    return scores

dossier = "./"
matrix, players, nb_songs = read()
scores = score_song(matrix,len(players),nb_songs)
ord = np.argsort(scores)
scores = scores[ord][::-1]

players = [players[i] for i in ord][::-1]

for id_,name in enumerate(players):
    print(f'Player {name} got {int(scores[id_])} points')